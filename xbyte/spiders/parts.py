# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class PartsSpider(CrawlSpider):
    name = 'parts'
    allowed_domains = ['xbyte.com']
    start_urls = ['https://www.xbyte.com/hw/dell/server-parts.html']

    deny_urls = [
        '\?',
        '#',
    ]

    rules = (
        Rule(LinkExtractor(allow=('hw/dell/server-parts'), deny=deny_urls)),
        Rule(LinkExtractor(allow=('catalog/product/view/id')), callback='parse_item'),
    )


    def parse_item(self, response):
        d = {}

        d['name'] = response.css('h1.page-title span ::text').extract_first()
        d['url'] = response.url
        d['breadcrumbs'] = dict(zip([t.strip() for t in response.css('.breadcrumbs ul li a::text').extract()], [t.strip() for t in response.css('.breadcrumbs ul li a::attr(href)').extract()]))
        d['code'] = response.css('.product-info-price [itemprop="sku"] ::text').extract_first()
        d['category'] = response.css('.product-info-price .catname ::text').extract_first()

        if not d['category']:
            d['category'] = ''

        d['description'] = response.css('.product-info-price .description .value ::text').extract_first()

        if d['description']:
            d['description'] = d['description'].replace('&nbsp;', ' ').strip()
        else:
            d['description'] = ''

        yield d
